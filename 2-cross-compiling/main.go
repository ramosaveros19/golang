package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Sistema operativo: %v\n Arquitectura: %v\n", runtime.GOOS, runtime.GOARCH)

}
