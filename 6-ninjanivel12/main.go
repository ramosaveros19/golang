package main

import (
	"fmt"

	"gitlab.com/ramosaveros19/golang.git/6-ninjanivel12/perro"
)

type canino struct {
	nombre string
	edad   int
}

func main() {
	c1 := canino{
		nombre: "Frandal",
		edad:   perro.Edad(4),
	}
	fmt.Println(c1)
}
