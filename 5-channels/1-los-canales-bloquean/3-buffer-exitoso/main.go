package main

import "fmt"

func main() {
	// unbuffered channel
	ca := make(chan int, 1)

	ca <- 42

	fmt.Println(<-ca)
}
