package main

import "fmt"

func main() {
	// unbuffered channel
	ca := make(<-chan int, 2) //canal receive only

	ca <- 42
	ca <- 43

	fmt.Println(<-ca)
	fmt.Println(<-ca)
	fmt.Println("-------")
	fmt.Printf("%T\n", ca)
}
