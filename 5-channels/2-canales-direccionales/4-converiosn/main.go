package main

import "fmt"

func main() {
	c := make(chan int)
	cr := make(<-chan int) // receive
	cs := make(chan<- int) // send

	fmt.Println("-------")
	fmt.Println("c\t%T\n", c)
	fmt.Println("cr\t%T\n", cr)
	fmt.Println("cs\t%T\n", cs)

	// expecífico a general no asigna
	// c = cr
	// c = cs

	// general a específico asigna
	cr = c
	cs = c

	// general a espcífico convierte
	fmt.Println("-------")
	fmt.Println("c\t%T\n", (<-chan int)(c))
	fmt.Println("c\t%T\n", (chan<- int)(c))

	// específico a general no convierte
	fmt.Println("-------")
	fmt.Println("c\t%T\n", (chan int)(cr))
	fmt.Println("c\t%T\n", (chan int)(cs))
}
